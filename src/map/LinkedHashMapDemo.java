package map;

import java.util.LinkedHashMap;

public class LinkedHashMapDemo {

	public static void main(String args[]) {

		LinkedHashMap map = new LinkedHashMap();

		map.put(1, "a");
		map.put(2, "a");
		map.put(3, "a");
		map.put(4, "a");
		map.put(5, "a");
		map.put(6, "a");
		map.put(7, "a");
		map.forEach((key, value) -> {
			System.out.println(key + "    " + value);

		});

	}
}
