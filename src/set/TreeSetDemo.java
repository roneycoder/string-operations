package set;

import java.util.HashSet;
import java.util.Set;

public class TreeSetDemo {

	public static void main(String args[])

	{
		Set t = new HashSet();
		t.add(66);
		t.add(2);
		t.add(3);
		t.add("a");
		t.add("b");
		t.add("c");
		System.out.println(t);
		t.forEach(object -> {
			System.out.println(object);
		});

	}

}
