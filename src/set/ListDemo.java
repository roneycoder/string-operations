package set;

import java.util.LinkedHashSet;

public class ListDemo {

	public static void main(String argsp[]) {

		LinkedHashSet list = new LinkedHashSet();
		list.add("Geeks");
		list.add("for");
		list.add("Geeks");
		list.add("GeeksForGeeks");
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);
		list.forEach(action -> {
			System.out.println(action);
		});

		System.out.println(list);
	}

}
