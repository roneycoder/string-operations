package map;

import java.util.HashMap;
import java.util.TreeMap;

public class HashMapDemo {

	public static void main(String args[]) {
		HashMap map = new HashMap();
		map.put(1, "abc");
		map.put(7, "abc");
		map.put(9, "abc");
		map.put(3, "abc");
		map.put(5555, "abc");
		map.put(5, "abc");
		HashMap map2 = new HashMap();
		map2.put(14, "abc");
		map2.put(13, "abc");
		map2.put(12, "abc");
		map2.put(11, "abc");
		map2.put(7, "abc");
		map2.put(6, "abc");
		HashMap h3 = new HashMap<>();

		map.forEach((key, value) -> {
			h3.put(key, value);

		});

		map2.forEach((key, value) -> {
			h3.put(key, value);

		});

		h3.forEach((key, value) -> {
			System.out.println("from h3" + key + " " + value);

		});

	}

}
