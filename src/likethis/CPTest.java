package likethis;

import java.util.HashMap;
import java.util.Map;

public class CPTest {
	
	public static void main(String[] args) {
		CPTest test=new CPTest();
		test.solution("alex", "bob");
	}
	
	  public int solution(String A, String B) {

			Map<String,Integer> key = new HashMap<String,Integer>();
			int win = 0;
			
			key.put("A", 14);
			key.put("K", 13);
			key.put("Q", 12);
			key.put("J", 11);
			key.put("10",10);
			key.put("9", 9);
			key.put("8", 8);
			key.put("7", 7);
			key.put("6", 6);
			key.put("5", 5);
			key.put("4", 4);
			key.put("3", 3);
			key.put("2", 2);
			key.put("1", 1);
			
			//System.out.println(key.get("A"));
			
		
			char[] a = A.toCharArray();
			char[] b = B.toCharArray();
			
			for(int f = 0;f<A.length();f++) {
				
				int avalue =0;
				int bvalue=0;
				
				if(key.containsKey(String.valueOf(a[f]))) {
					
					avalue= key.get(String.valueOf(a[f]));
				}
				if(key.containsKey(String.valueOf(b[f]))) {
					bvalue = key.get(String.valueOf(b[f]));
				}
				if(avalue!=0 && bvalue!=0 && avalue>bvalue ) {
					win = win +1;
				}
				
			}
			
			
			return win;
			       // write your code in Java S
}	
}