package comparable;

public class employee implements Comparable<employee> {

	int age;
	String name;
	String lname;

	
	
 employee(int i, String string, String string2) {
		// TODO Auto-generated constructor stub
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	@Override
	public int compareTo(employee e) {
		if (this.getAge() > e.getAge()) {
			return 1;
		}

		else {
			return -1;
		}

	}

	@Override
	public String toString() {
		return "employee [age=" + age + ", name=" + name + ", lname=" + lname + "]";
	}

}
