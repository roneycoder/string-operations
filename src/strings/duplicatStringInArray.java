package strings;

import java.util.HashSet;
import java.util.Set;

public class duplicatStringInArray {

	public static void main(String args[]) {

		String[] strArray = { "Java", "JSP", "Servlets", "Java", "Struts", "JSP", "JDBC" };
		Set set = new HashSet();

		for (String str : strArray) {
			if (set.add(str) == false) {
				System.out.println("Duplicate i s" + str);

			}
		}
	}

}
