package comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class main {

	public static void main(String[] args) {

		// List<employee> list = new ArrayList();
		//
		// list.add(new employee(18, "fname", "lname"));
		//
		// list.add(new employee(10, "fname", "lname"));
		// list.add(new employee(8, "fname", "lname"));
		// list.add(new employee(22, "fname", "lname"));
		// list.add(new employee(1, "fname", "lname"));
		// System.out.println(list);
		// Collections.sort(list);
		// for (employee e : list) {
		// System.out.println(e);
		// }
		//
		List<laptop> list = new ArrayList();

		list.add(new laptop(102, "150", "hp"));

		list.add(new laptop(105, "200", "ab"));
		list.add(new laptop(102, "200", "hp"));
		list.add(new laptop(105, "200", "aa"));
		list.add(new laptop(101, "200", "hp"));

		Collections.sort(list);
		System.out.println(list);
		for (laptop l : list) {
			System.out.println(l);
		}

	}
}