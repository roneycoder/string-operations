package overloading;

public class main {

	public static void main(String args[]) {

		A a = new A();
		System.out.println(a.add(10, 20));
		System.out.println(a.add(10f, 20f));
		System.out.println(a.add(10.2, 20.2, 30.0));

	}
}
