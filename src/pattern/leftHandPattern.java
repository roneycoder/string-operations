package pattern;

public class leftHandPattern {

	public static void main(String args[]) {
		//
		// int value = 3;
		// int count = 1;
		// for (int i = 0; i < value; i++) {
		// for (int j = 0; j <= i; j++) {
		// System.out.print(count + " ");
		//
		// count++;
		// }
		//
		// System.out.println();
		// }

		printarray();

	}

	public static void printarray() {
		int arr[] = { 1, 2, 3, 4, 5, 6 };
		int count = 0;

		for (int i = 1; i < arr.length - 1; i++) {
			for (int j = 0; j < i - 1; j++) {
				System.out.print(arr[count] + " ");
				count++;

			}
			System.out.println();
		}

	}
}
