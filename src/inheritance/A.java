package inheritance;

public abstract class A {

	public void print() {
		System.out.println("in a");
	}

	public abstract void test();

}

class b extends A {

	// public void print() {
	// System.out.println("in b");
	// }

	public void printa() {
		System.out.println("in b");
		A a = new b();
	}

	@Override
	public void test() {
		// TODO Auto-generated method stub

	}

}
