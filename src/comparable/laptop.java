package comparable;

import java.util.Comparator;

public class laptop implements Comparable<laptop> {

	private int id;
	private String price;
	private String brand;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	@Override
	public String toString() {
		return "laptop [id=" + id + ", price=" + price + ", brand=" + brand + "]";
	}

	public laptop(int id, String price, String brand) {
		super();
		this.id = id;
		this.price = price;
		this.brand = brand;
	}

	@Override
	public int compareTo(laptop laptop) {
		if (this.getId() > laptop.getId()) {
			return 1;
		} else {
			return -1;
		}
	}
}
