package strings;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class StringDuplicate {

	public static void main(String args[]) {

		String str = "aabsdassdfggz";
		char ch[] = str.toCharArray();

		Set<Character> set = new HashSet<>();

		for (char c : ch) {
			if (set.add(c) == false) {
				System.out.println("Duplicate is +" + c);
			}
		}
		// map();

	}

	public static void map() {

		String str = "sdasfadaserqqw";
		char ch[] = str.toCharArray();
		Map<Character, Integer> m = new HashMap();

		for (char c : ch) {
			if (m.containsKey(c)) {
				m.put(c, m.get(c) + 1);

			} else {
				m.put(c, 1);
			}
		}
		System.out.println(m);

	}

}
